package com.csi.los.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.csi.los.R;
import com.csi.los.Utility.Constants;
import com.csi.los.Utility.Utility;

public class ActivityLoanCaseEntry extends AppCompatActivity {
    Button buttonNext,buttonCommit;
    EditText editTextBranchName,editTextNoOfBorrowers,editTextInterestRate,editTextLoanAmount,editTextLoanTenor;
    Context mContext=this;
    String branchName,appliedLoanAmount,appliedLLoanTenor,noOfBorrowers,interestrate;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_case_entry);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        setUpUI();
        clickListener();

        Typeface typefaceRegular = Typeface.createFromAsset(getAssets(),"font/San_Francisco_Regular.ttf");
        Typeface typefaceBold = Typeface.createFromAsset(getAssets(),"font/San_Francisco_Bold.ttf");
        Typeface typefaceThin = Typeface.createFromAsset(getAssets(),"font/San_Francisco_Thin.ttf");
        buttonNext.setTypeface(typefaceBold);
    }

    private Context getActivity() {
        return this;
    }

    private void clickListener() {
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(ActivityLoanCaseEntry.this,ActivityPersonalInfo.class);
               startActivity(intent);
               //overridePendingTransition(R.anim.left_to_right,R.anim.left_to_right);

            }
        });
       /* buttonCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Constants.SharedprefItem.BRANCH_NAME, editTextLoanAmount.getText().toString());

                    editor.commit();
                    editTextNoOfBorrowers.setText(sharedPreferences.getString(Constants.SharedprefItem.BRANCH_NAME, ""));
                }catch (Exception e){}

                Utility.putString(mContext,Constants.SharedprefItem.BRANCH_NAME,editTextLoanAmount.getText().toString());
                editTextNoOfBorrowers.setText(Utility.getString(mContext,Constants.SharedprefItem.BRANCH_NAME));

            }
        });
        */
    }
    private void setUpUI() {
        buttonNext = (Button) findViewById(R.id.buttonNext);
        editTextBranchName = (EditText) findViewById(R.id.editTextBranchName);
        editTextInterestRate = (EditText) findViewById(R.id.editTextInterestrate);
        editTextNoOfBorrowers = (EditText) findViewById(R.id.editTextBorrowerNo);
        editTextLoanAmount = (EditText) findViewById(R.id.editTextAppliedLoanAmount);
        editTextLoanTenor = (EditText) findViewById(R.id.editTextAppliedLoanTenor);
    }

}
