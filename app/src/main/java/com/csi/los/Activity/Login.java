package com.csi.los.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.csi.los.R;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

public class Login extends AppCompatActivity {
    CheckBox checkBoxShowPassword;
    EditText password,username;
    TextView textViewForgetPassword,textViewLoginTitle,textViewLoginDes;
    Button buttonLogin;
    ImageView imageViewLogo;
    Animation animation;
    ShowHidePasswordEditText showHidePasswordEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initUI();
        setAnimation();

        Typeface typefaceThin = Typeface.createFromAsset(getAssets(),"font/San_Francisco_Thin.ttf");
        Typeface typefaceBold = Typeface.createFromAsset(getAssets(),"font/San_Francisco_Bold.ttf");

        buttonLogin.setTypeface(typefaceBold);
        textViewLoginTitle.setTypeface(typefaceBold);
        textViewLoginDes.setTypeface(typefaceThin);
        //textViewForgetPassword.setTypeface(typeface);
        //show password
       /*
        checkBoxShowPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    password.setInputType(InputType.TYPE_CLASS_TEXT);
                    password.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    password.setInputType(InputType.TYPE_CLASS_TEXT
                            | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    password.setTransformationMethod(PasswordTransformationMethod .getInstance());
                }
            }
        });
*/
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, ActivityHome.class);
                startActivity(intent);
            }
        });

    }
    private void setAnimation() {
        animation= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.bounce);
        //imageViewLogo.setAnimation(animation);
    }
    private void initUI() {
        username = (EditText) findViewById(R.id.userId);
        buttonLogin = (Button) findViewById(R.id.login);
        imageViewLogo = (ImageView) findViewById(R.id.logo);
        textViewForgetPassword = (TextView) findViewById(R.id.forget_password);
        textViewLoginTitle = (TextView) findViewById(R.id.textViewLoginTitle);
        textViewLoginDes = (TextView) findViewById(R.id.textViewLoginTitleDes);
        showHidePasswordEditText = (ShowHidePasswordEditText) findViewById(R.id.password);
    }
}
