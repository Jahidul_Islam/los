package com.csi.los.Activity;

import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.csi.los.R;

public class ActivitySignUp extends AppCompatActivity {
    EditText editTextEmail,editTextPassword,editTextConfirmPassword;
    Button buttonSignUp,buttonLogin;
    TextView textViewSignUp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setUpUI();
        onClick();
        Typeface typeface = Typeface.createFromAsset(getAssets(),"font/lobster.ttf");
        //password matching
        /*editTextConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                String password1,password2;
                password1 = editTextPassword.getText().toString();
                password2 = editTextConfirmPassword.getText().toString();
                if(password1.equals(password2)){
                }
                else {
                    editTextConfirmPassword.setError("Password Do Not Match");
                    editTextConfirmPassword.requestFocus();
                }
            }
        });
        */
        /*buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String password1,password2,email;
                try {
                    password1 = editTextPassword.getText().toString();
                    password2 = editTextConfirmPassword.getText().toString();
                    email = editTextEmail.getText().toString();
                    if (TextUtils.isEmpty(email)) {
                        editTextEmail.setError("Enter Email address");
                        editTextEmail.requestFocus();
                    } else if (TextUtils.isEmpty(password1)) {
                        editTextPassword.setError("Enter Password");
                        editTextPassword.requestFocus();
                    } else if (TextUtils.isEmpty(password2)) {
                        editTextConfirmPassword.setError("Re-Enter Password");
                        editTextConfirmPassword.requestFocus();
                    } else if (password1.equals(password2)) {
                    } else {
                        editTextConfirmPassword.setError("Password do not match");
                        editTextConfirmPassword.requestFocus();
                    }
                }catch (Exception e){}
            }
        });
        */
    }

    private void onClick() {
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void setUpUI() {
        buttonLogin = (Button) findViewById(R.id.buttonLogin);

    }
}
