package com.csi.los.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.csi.los.R;

public class ActivityHome extends AppCompatActivity {
    Toolbar toolbar;
    LinearLayout linearLayoutLoanCaseEntry,linearLayoutSearch,linearLayoutNotInitiatedLoans;
    TextView textViewLoanCaseEntry,textViewBankAgentName,textViewExistingLoanTitle,textViewNotInitiatedLoan;
    EditText editTextSearch;
    ImageView imageViewSetting;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setUpUI();
        clickListstener();
        Typeface typefaceRegular = Typeface.createFromAsset(getAssets(),"font/San_Francisco_Regular.ttf");
        Typeface typefaceBold = Typeface.createFromAsset(getAssets(),"font/San_Francisco_Bold.ttf");
        Typeface typefaceThin = Typeface.createFromAsset(getAssets(),"font/San_Francisco_Thin.ttf");
        textViewBankAgentName.setTypeface(typefaceRegular);
        textViewExistingLoanTitle.setTypeface(typefaceThin);
        textViewLoanCaseEntry.setTypeface(typefaceRegular);
        textViewNotInitiatedLoan.setTypeface(typefaceRegular);
    }

    private void clickListstener() {
        linearLayoutLoanCaseEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityHome.this,ActivityLoanCaseEntry.class);
                startActivity(intent);
            }
        });
        linearLayoutSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityHome.this,ActivityLoanSearch.class);
                startActivity(intent);
            }
        });
        editTextSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityHome.this,ActivityLoanSearch.class);
                startActivity(intent);
            }
        });
        imageViewSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityHome.this,ActivityAgentAccount.class);
                startActivity(intent);
            }
        });
        linearLayoutNotInitiatedLoans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityHome.this,ActivityNotInitiatedLoan.class));
            }
        });
    }

    private void setUpUI() {
        linearLayoutLoanCaseEntry = (LinearLayout) findViewById(R.id.linearLoanCaseEntry);
        linearLayoutNotInitiatedLoans = (LinearLayout) findViewById(R.id.linearNotInitiationLoans);
        linearLayoutSearch = (LinearLayout) findViewById(R.id.linearlayoutSearch);
        textViewLoanCaseEntry = (TextView) findViewById(R.id.textViewLoanCaseEntry);
        textViewBankAgentName = (TextView) findViewById(R.id.textViewBankAgentUserName);
        textViewExistingLoanTitle = (TextView) findViewById(R.id.textViewExistingLoanSearch);
        textViewNotInitiatedLoan = (TextView) findViewById(R.id.textViewNotInitiationLoans);
        editTextSearch = (EditText) findViewById(R.id.editTextSearch);
        imageViewSetting = (ImageView) findViewById(R.id.imageViewAgentSetting);
    }


}
