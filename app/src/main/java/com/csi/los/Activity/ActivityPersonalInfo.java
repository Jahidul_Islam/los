package com.csi.los.Activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.csi.los.R;
import com.csi.los.Utility.MonthToText;

import java.util.Calendar;

public class ActivityPersonalInfo extends AppCompatActivity {
    Button buttonNext;
    Spinner spinnerGender,spinnerMaritalStatus,spinnerEducationLevel,spinnerCountry,spinnerCity,spinnerParmanentCountry,spinnerParmanentCity;
    CheckBox checkBoxSameAddress;
    EditText editTextPermanentAddress,editTextPermanentPostCode,editTextPermanentLivingPeriodYear,editTextPermanentLivingPeriodMonth,editTextDateOfBirth;
    //calender
    Calendar calendar;
    int year,day,month;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setupUI();
        spinnerDropDown();
        checkBoxClick();
        selectDate();
        buttonNext = (Button) findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityPersonalInfo.this,ActivityFamilyInfo.class));
            }
        });
    }

    private void selectDate() {
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DATE);

        editTextDateOfBirth.setText(MonthToText.mothNameText(day + "-" + (month + 1) + "-" + year));
        editTextDateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int mYear, int monthOfYear, int dayOfMonth) {
                        editTextDateOfBirth.setText(MonthToText.mothNameText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + mYear));
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });
    }

    private Context getActivity() {
        return this;
    }

    private void checkBoxClick() {
        checkBoxSameAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    try {
                        editTextPermanentAddress.setEnabled(false);
                        editTextPermanentPostCode.setEnabled(false);
                        editTextPermanentLivingPeriodYear.setEnabled(false);
                        editTextPermanentLivingPeriodMonth.setEnabled(false);
                        spinnerParmanentCountry.setEnabled(false);
                        spinnerParmanentCity.setEnabled(false);
                    }catch (Exception e){
                    }
                }
                else {
                    try {
                        editTextPermanentAddress.setEnabled(true);
                        editTextPermanentPostCode.setEnabled(true);
                        editTextPermanentLivingPeriodYear.setEnabled(true);
                        editTextPermanentLivingPeriodMonth.setEnabled(true);
                        spinnerParmanentCountry.setEnabled(true);
                        spinnerParmanentCity.setEnabled(true);
                    }catch (Exception e){}
                }
            }
        });
    }
    private void spinnerDropDown() {
        ArrayAdapter gender = ArrayAdapter.createFromResource(this, R.array.GENDER, R.layout.spinner_item);
        gender.setDropDownViewResource(R.layout.spinner_list);
        spinnerGender.setAdapter(gender);
        ArrayAdapter city = ArrayAdapter.createFromResource(this, R.array.CITY, R.layout.spinner_item);
        city.setDropDownViewResource(R.layout.spinner_list);
        spinnerCity.setAdapter(city);
        ArrayAdapter country = ArrayAdapter.createFromResource(this, R.array.COUNTRY, R.layout.spinner_item);
        country.setDropDownViewResource(R.layout.spinner_list);
        spinnerCountry.setAdapter(country);
        ArrayAdapter maritalStatus = ArrayAdapter.createFromResource(this, R.array.MARITAL_STATUS, R.layout.spinner_item);
        maritalStatus.setDropDownViewResource(R.layout.spinner_list);
        spinnerMaritalStatus.setAdapter(maritalStatus);
        ArrayAdapter educationLevel = ArrayAdapter.createFromResource(this, R.array.EDUCATION, R.layout.spinner_item);
        educationLevel.setDropDownViewResource(R.layout.spinner_list);
        spinnerEducationLevel.setAdapter(educationLevel);
        ArrayAdapter permanentCountry = ArrayAdapter.createFromResource(this, R.array.COUNTRY, R.layout.spinner_item);
        permanentCountry.setDropDownViewResource(R.layout.spinner_list);
        spinnerParmanentCountry.setAdapter(permanentCountry);
        ArrayAdapter permanentCity = ArrayAdapter.createFromResource(this, R.array.CITY, R.layout.spinner_item);
        permanentCity.setDropDownViewResource(R.layout.spinner_list);
        spinnerParmanentCity.setAdapter(permanentCity);
    }
    private void setupUI() {
        spinnerGender = (Spinner) findViewById(R.id.spinnerGender);
        spinnerMaritalStatus = (Spinner) findViewById(R.id.spinnerMaritalStatus);
        spinnerEducationLevel = (Spinner) findViewById(R.id.spinnerEducationLevel);
        spinnerCountry = (Spinner) findViewById(R.id.spinnerCountry);
        spinnerCity = (Spinner) findViewById(R.id.spinnerCity);
        spinnerParmanentCountry = (Spinner) findViewById(R.id.spinnerPermanentCountry);
        spinnerParmanentCity = (Spinner) findViewById(R.id.spinnerPermanentCity);
        checkBoxSameAddress = (CheckBox) findViewById(R.id.chekboxSameAsResidence);
        editTextPermanentAddress = (EditText) findViewById(R.id.editTextPermanentAddress);
        editTextPermanentPostCode = (EditText) findViewById(R.id.editTextPermanentPostCode);
        editTextPermanentLivingPeriodYear = (EditText) findViewById(R.id.editTextPermanentYears);
        editTextPermanentLivingPeriodMonth = (EditText) findViewById(R.id.editTextPermanentMonth);
        editTextDateOfBirth = (EditText) findViewById(R.id.editTextDateOfBirth);
    }
}
