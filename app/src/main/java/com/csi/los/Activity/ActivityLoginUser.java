package com.csi.los.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.csi.los.R;

public class ActivityLoginUser extends AppCompatActivity {
    TextView textViewSignUp,textViewForgetPassword;
    CheckBox checkBoxShowPassword;
    EditText password,username;
    Button buttonLogin,buttonSignUp;
    ImageView imageViewLogo;
    Animation animation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_user);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setupUI();
        OnClick();
        setAnimation();

        Typeface typeface = Typeface.createFromAsset(getAssets(),"font/dancing_script.ttf");
    }
    private void setAnimation() {
        animation= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.bounce);
    }

    private void OnClick() {
        try {
            buttonSignUp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ActivityLoginUser.this,ActivitySignUp.class);
                    startActivity(intent);
                }
            });
        }catch (Exception e){}

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityLoginUser.this,ActivityUserProfile.class);
                startActivity(intent);
            }
        });
    }
    private void setupUI() {
        password = (EditText) findViewById(R.id.password);
        username = (EditText) findViewById(R.id.userId);
        buttonLogin = (Button) findViewById(R.id.login);
        imageViewLogo = (ImageView) findViewById(R.id.logo);
        textViewForgetPassword = (TextView) findViewById(R.id.forget_password);
        buttonSignUp = (Button) findViewById(R.id.buttonSignUp);
    }
}
