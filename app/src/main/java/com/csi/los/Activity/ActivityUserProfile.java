package com.csi.los.Activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.csi.los.R;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityUserProfile extends AppCompatActivity {
    CircleImageView circleImageViewProfile;
    ScrollView scrollViewDown;
    LinearLayout linearLayoutApplyForLoan,linearLayoutLoanStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initUI();
        ClickListener();
    }
    private void ClickListener() {
        linearLayoutApplyForLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityUserProfile.this,ActivityApplyForLoan.class));
            }
        });
        circleImageViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityUserProfile.this,ActivityEditProfile.class));
            }
        });
        linearLayoutLoanStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityUserProfile.this,ActivityLoanStatus.class));
            }
        });
    }
    private void initUI() {
        linearLayoutApplyForLoan = (LinearLayout) findViewById(R.id.linearApplyForLoan);
        linearLayoutLoanStatus = (LinearLayout) findViewById(R.id.linearLoanStatus);
        circleImageViewProfile = (CircleImageView) findViewById(R.id.imageViewProfile);
    }
}
