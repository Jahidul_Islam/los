package com.csi.los.Utility;

/**
 * Created by Jahid on 5/22/17.
 */

public class Constants {

    private static int SPLASH_TIME_OUT = 1500;
    private static String CITY = "CITY";

    public class SharedprefItem {
        public static final String BRANCH_NAME = "BRANCH_NAME";
        public static final String BRANCH_NAME1 = "BRANCH_NAME1";
        public static final String CUSTOMER_CODE = "CUSTOMER_CODE";
        public static final String FULL_NAME = "FULL_NAME";

    }

}
